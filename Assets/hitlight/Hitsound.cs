using UnityEngine;

public class Hitsound : MonoBehaviour
{
    public AudioSource hitmarker;

    private void OnCollisionEnter(Collision collision)
    {
        hitmarker.Play();
    }
}
