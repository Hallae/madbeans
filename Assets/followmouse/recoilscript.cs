using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recoilscript : MonoBehaviour
{
    public GameObject Gun;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
       if (Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(StartRecoil());
        }
    }

    IEnumerator StartRecoil()
    {
        Gun.GetComponent<Animator>().Play("Recoil");
        yield return new WaitForSeconds(0.20f);
        Gun.GetComponent<Animator>().Play("New State");
    }
    public void Fire()
    {
        Gun.GetComponent<Animator>().SetTrigger("Fire");
    }
}
