using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launchprojectile : MonoBehaviour
{
    public GameObject projectile;
    public float launchVelocity = 2600f;
    public float damage = 35f;
    public float range = 5000f;
    public float impactForce = 200f;
    public float bulletspeed = 55f;
    public ParticleSystem MuzzleFlash;
   

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject ball = Instantiate(projectile, transform.position,
                                                     transform.rotation);
            ball.GetComponent<Rigidbody>().AddRelativeForce(new Vector3
                                               (0, 0, launchVelocity));

        }


       
        if (Input.GetButtonDown("Fire1"))
        {
            MuzzleFlash.Play();
        }

        

    }
}
